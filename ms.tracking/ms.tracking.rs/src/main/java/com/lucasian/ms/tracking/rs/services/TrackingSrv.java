package com.lucasian.ms.tracking.rs.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lucasian.ms.tracking.bean.TrackingBean;
import com.lucasian.ms.tracking.common.model.TrackingMessage;

@Path("/tracking-srv")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TrackingSrv {
	
	@PUT
	@Path("track")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response track( TrackingMessage message ) {
		
		try {
			TrackingBean bean = new TrackingBean();
			bean.track(message);
			
		}catch(Exception e) {
			return Response.status(201).entity("Executed").build();	
		}
		
		return Response.status(201).entity("Executed").build();
	}
}

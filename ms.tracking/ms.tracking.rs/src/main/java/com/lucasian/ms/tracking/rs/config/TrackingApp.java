package com.lucasian.ms.tracking.rs.config;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.lucasian.ms.tracking.rs.services.TrackingSrv;

public class TrackingApp extends Application{
	
	@Override
	public Set<Class<?>> getClasses() {
		
		final Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add( TrackingSrv.class );
		return classes;
	}
}

package com.lucasian.ms.tracking.common.util;

import java.util.UUID;

public final class CorrelationalGeneratorUtil {
	
	public final static String getCorrelationalId() {
		java.util.UUID uuid = UUID.randomUUID();
		return String.valueOf( uuid );
	}
}

package com.lucasian.ms.tracking.common.util;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
	
	
	protected final static String RESOURCE_FILE_NAME= "tracking.properties";
	
	public static String getProperty( String key ) {
		String propertyValue = null;
		InputStream is = null;
		try {
			
			Properties properties = new Properties();
			is = PropertiesUtil.class.getClassLoader().getResourceAsStream( RESOURCE_FILE_NAME );
			if(is == null ) {
				throw new Exception("Error loading resource file from ms.tracking.common/src/main/resources");
			}
			properties.load( is );
			propertyValue = properties.getProperty( key );
			if(propertyValue == null) {
				throw new Exception("Error getting propertu " + key);
			}
			
		}catch(Exception e){
			
		}finally {
			try {
				is.close();	
			}catch(Exception e) {
				
			}
			
		}
		return propertyValue;
	}

}

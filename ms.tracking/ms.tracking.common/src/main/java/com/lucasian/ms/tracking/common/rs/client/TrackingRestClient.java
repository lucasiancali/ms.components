package com.lucasian.ms.tracking.common.rs.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.Gson;
import com.lucasian.ms.tracking.common.model.TrackingMessage;
import com.lucasian.ms.tracking.common.util.PropertiesUtil;

public class TrackingRestClient {
	
	/**
	 * 
	 * @param msg
	 */
	public static void callService( final TrackingMessage msg ) {
		DefaultHttpClient httpClient = null;
		try {

			httpClient = new DefaultHttpClient();
			String serviceEndPoint =  PropertiesUtil.getProperty( "tracking.service.url" );
			HttpPost postRequest = new HttpPost(serviceEndPoint);
			
			String jsonMessage = convertToJson(msg);
			StringEntity input = new StringEntity(jsonMessage);
			input.setContentType("application/json");
			postRequest.setEntity(input);
			HttpResponse response = httpClient.execute(postRequest);

			if (response.getStatusLine().getStatusCode() != 201) {
				throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader( new InputStreamReader((response.getEntity().getContent())));

			String output;
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}

		} catch (MalformedURLException e) {
			  e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			httpClient.getConnectionManager().shutdown();
		}
	}
	
	/**
	 * 
	 * @param msg
	 * @return
	 */
	private static String convertToJson( final TrackingMessage msg ) {
		if(msg == null) {
			return null;
		}
		Gson gson = new Gson();
		String jsonMsg =  gson.toJson( msg );
		return jsonMsg;
	}
}

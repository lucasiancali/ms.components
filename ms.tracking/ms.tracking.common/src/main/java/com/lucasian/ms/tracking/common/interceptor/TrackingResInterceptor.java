package com.lucasian.ms.tracking.common.interceptor;

import java.io.IOException;
import java.util.Calendar;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;

import com.lucasian.ms.tracking.common.model.TrackingMessage;
import com.lucasian.ms.tracking.common.rs.client.TrackingRestClient;

public class TrackingResInterceptor implements ContainerRequestFilter{

	public void filter(ContainerRequestContext arg0) throws IOException {
		
		TrackingMessage msg = new TrackingMessage();
		msg.setTimestamp( Calendar.getInstance().getTimeInMillis() );
		TrackingRestClient.callService( msg );
	}

}

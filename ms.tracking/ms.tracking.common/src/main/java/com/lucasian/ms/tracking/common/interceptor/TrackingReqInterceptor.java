package com.lucasian.ms.tracking.common.interceptor;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;

import com.lucasian.ms.tracking.common.model.TrackingMessage;
import com.lucasian.ms.tracking.common.rs.client.TrackingRestClient;
import com.lucasian.ms.tracking.common.util.CorrelationalGeneratorUtil;

public class TrackingReqInterceptor implements ContainerRequestFilter{
	
	@Context
	private HttpServletRequest req;
	 
	public void filter(ContainerRequestContext crc) throws IOException {
		
		TrackingMessage msg = new TrackingMessage();
		
		msg.setCorrelationId( CorrelationalGeneratorUtil.getCorrelationalId()  );
		msg.setTimestamp( Calendar.getInstance().getTimeInMillis() );
		msg.setIp( req.getRemoteAddr() );
	
		TrackingRestClient.callService( msg );
	}

}

package com.lucasian.ms.tracking.mngr;

import com.lucasian.ms.tracking.TrackingBaseStore;
import com.lucasian.ms.tracking.TrackingCommon;
import com.lucasian.ms.tracking.TrackingStoreFactory;
import com.lucasian.ms.tracking.TrackingStoreType;
import com.lucasian.ms.tracking.common.model.TrackingMessage;

public class TrackingMngr extends TrackingCommon{
	
	public void track( final TrackingMessage trackingMessage ) {
		this.enqueueMsg(trackingMessage);
	}
	
	public void processMessage( final TrackingMessage trackingMessage ) {
		TrackingBaseStore base = TrackingStoreFactory.getStoreInstance( TrackingStoreType.CASSANDRA );
		base.store(trackingMessage);
		
	}
	
}

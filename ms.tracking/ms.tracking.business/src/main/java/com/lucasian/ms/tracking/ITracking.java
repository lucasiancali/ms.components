package com.lucasian.ms.tracking;

import com.lucasian.ms.tracking.common.model.TrackingMessage;

public interface ITracking {
	
	public void track(TrackingMessage message);

}

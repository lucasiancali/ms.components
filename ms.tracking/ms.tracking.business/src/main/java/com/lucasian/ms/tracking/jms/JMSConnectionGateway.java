package com.lucasian.ms.tracking.jms;

import javax.jms.JMSException;
import javax.jms.QueueSender;
import javax.jms.QueueSession;

public class JMSConnectionGateway {
	
	QueueSender queueSender = null;
	QueueSession queueSession = null;
	
	public QueueSender getQueueSender() {
		return queueSender;
	}
	public void setQueueSender(QueueSender queueSender) {
		this.queueSender = queueSender;
	}
	public QueueSession getQueueSession() {
		return queueSession;
	}
	public void setQueueSession(QueueSession queueSession) {
		this.queueSession = queueSession;
	}
	
	public void close() throws JMSException{
		
		if(queueSender != null){
			queueSender.close();
		}
		
		if(queueSession != null){
			queueSession.close();
		}
	}
}

package com.lucasian.ms.tracking.bean;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import com.lucasian.ms.tracking.ITracking;
import com.lucasian.ms.tracking.TrackingCommon;
import com.lucasian.ms.tracking.common.model.TrackingMessage;
import com.lucasian.ms.tracking.mngr.TrackingMngr;

@Stateless(mappedName = "TrackingBean", name = "TrackingBean", description="TrackingBean")
@Remote(TrackingCommon.class)
public class TrackingBean implements ITracking{

	@Override
	public void track(TrackingMessage message) {
		try {
			TrackingCommon mngr = new TrackingMngr();
			mngr.track(message);
		}catch(Exception e) {
		}
	}

}

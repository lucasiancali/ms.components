package com.lucasian.ms.tracking.bean;

import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.lucasian.ms.tracking.common.model.TrackingMessage;
import com.lucasian.ms.tracking.mngr.TrackingMngr;

@MessageDriven(name = "TrackingMDB", mappedName = "TrackingMDB")
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class TrackingMDB implements MessageListener{

	
	@Override
	public void onMessage(Message message) {
		try {
			
            if (message instanceof ObjectMessage) {
            	
                ObjectMessage msg = (ObjectMessage) message;
                TrackingMessage trackingMessage = (TrackingMessage)msg.getObject();
                TrackingMngr mngr = new TrackingMngr();
                mngr.processMessage(trackingMessage);
            }
            
		}catch(JMSException jms) {
			
		}
	}
	

}

package com.lucasian.ms.tracking.jms;

import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.InitialContext;

public class JmsConnectionFactory {
	
	final static String DEFAULT_WF_CONNECTION_FACTORY = "java:jboss/exported/jms/RemoteConnectionFactory";
	
	public static JMSConnectionGateway getJMSQueueConnection(String jndiQueueName) throws Exception {
		return getJMSQueueConnection(jndiQueueName, DEFAULT_WF_CONNECTION_FACTORY);
	}
	
	public static JMSConnectionGateway getJMSQueueConnection(String jndiQueueName, String cfJndiName) throws Exception {
		
		QueueConnectionFactory qcf = null;
		Queue queue = null;
		QueueConnection qcon = null;
		QueueSession qsession = null;
		QueueSender qsender = null;
		JMSConnectionGateway jmsConnectionGateway = null;
		try{
			
			jmsConnectionGateway = new JMSConnectionGateway();
			InitialContext context = new InitialContext();
			
			qcf = (QueueConnectionFactory) context.lookup(cfJndiName);
			queue = (Queue) context.lookup(jndiQueueName);
			qcon = qcf.createQueueConnection();
			qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			qsender = qsession.createSender(queue);
			jmsConnectionGateway.setQueueSender(qsender);
			jmsConnectionGateway.setQueueSession(qsession);
			
		}catch(Exception e){
			
		}finally{
			try{
				
				qsender.close();
			}catch(Exception e){
			
			}
			try{
				qsession.close();				
			}catch(Exception e){
			
			}
			try{
				qcon.close();
			}catch(Exception e){
			}
		}
		
		return jmsConnectionGateway;
	}
}

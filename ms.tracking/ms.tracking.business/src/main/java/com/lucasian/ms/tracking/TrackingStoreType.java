package com.lucasian.ms.tracking;

public enum TrackingStoreType {
	
	CASSANDRA,
	LEAF,
	THRID_PARTY;
	
	private TrackingStoreType() {
		
	}
}

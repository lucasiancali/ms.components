package com.lucasian.ms.tracking;

import com.lucasian.ms.tracking.common.model.TrackingMessage;

public abstract class TrackingBaseStore {
	
	public abstract void store( final TrackingMessage trackingMessage);
	
}

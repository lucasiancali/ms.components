package com.lucasian.ms.tracking;

import javax.jms.ObjectMessage;

import com.lucasian.ms.tracking.common.model.TrackingMessage;
import com.lucasian.ms.tracking.jms.JMSConnectionGateway;
import com.lucasian.ms.tracking.jms.JmsConnectionFactory;

public abstract class TrackingCommon  implements ITracking{
	
	public abstract void track(TrackingMessage message);
	
	protected void enqueueMsg( final TrackingMessage trackingMessage ) {
		try {
			String queueName = "";
			JMSConnectionGateway gateway = JmsConnectionFactory.getJMSQueueConnection(queueName);
			ObjectMessage msg = gateway.getQueueSession().createObjectMessage(trackingMessage);
			gateway.getQueueSender().send(msg);
		}catch(Exception e) {
			
		}
	}
}
